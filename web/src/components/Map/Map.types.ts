import { LatLng } from '@/types';
import { MapContainerProps } from 'react-leaflet';
import { BusStop } from '~/types';

export type MapProps = Pick<MapContainerProps, 'center' | 'zoom'> & {
  busStops?: BusStop[];
  onDragEnd?: (center: LatLng) => void;
};
