import { MapContainer, Marker, Popup, TileLayer, useMap, useMapEvent } from 'react-leaflet';

import { useEffect } from 'react';
import { MapProps } from './Map.types';

export default function MapComponent(props: MapProps) {
  const { center = [1.3427, 103.8369], zoom = 16, onDragEnd } = props;

  const busStopMarkers =
    props.busStops?.map?.((busStop) => (
      <Marker key={busStop.geohash} position={[busStop.coordinates.lat, busStop.coordinates.long]}>
        <Popup>{busStop.name}</Popup>
      </Marker>
    )) ?? [];

  return (
    <MapContainer
      center={center}
      zoom={zoom}
      style={{
        width: '100%',
        height: '100%',
      }}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {busStopMarkers}
      <SetViewOnClick />
      <SetDragHandler onDragEnd={onDragEnd} />
      <SetViewOnStateChange center={center} />
    </MapContainer>
  );
}

function SetViewOnClick() {
  const map = useMapEvent('click', (e) => {
    map.setView(e.latlng, map.getZoom(), {
      animate: true,
    });
  });

  return null;
}

function SetDragHandler({ onDragEnd }) {
  const map = useMapEvent('dragend', () => {
    onDragEnd?.([map.getCenter().lat, map.getCenter().lng]);
  });

  return null;
}

function SetViewOnStateChange({ center }) {
  const map = useMap();

  useEffect(() => {
    map.setView(center, map.getZoom());
  }, [center, map]);

  return null;
}
