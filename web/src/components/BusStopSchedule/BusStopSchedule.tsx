import { makeFetchApi } from '@/utils/api';
import { formatDistanceToNow } from 'date-fns';
import { useEffect, useState } from 'react';
import { BusStopScheduleService } from '~/types';
import styles from './BusStopSchedule.module.css';
import { BusStopScheduleProps } from './BusStopSchedule.types';

export default function BusStopSchedule(props: BusStopScheduleProps) {
  const { busStop, onClose } = props;

  const [schedules, setSchedules] = useState<BusStopScheduleService[]>([]);

  useEffect(() => {
    (async () => {
      if (busStop) {
        const getSchedule = makeFetchApi(`bus-stops/${busStop.id}`);

        const result = await getSchedule();

        setSchedules(result.services);
      }
    })();
  }, [busStop]);

  const cards = schedules.map((schedule) => (
    <div className={styles.busStopScheduleListItem} key={schedule.bus.no}>
      <div className={styles.busStopScheduleListItemBusNo}>{schedule.bus.no}</div>
      <div className={styles.busStopScheduleListItemInfo}>
        <h5 className="bus-name">{schedule.bus.name}</h5>
        <p className="arrival-time">
          Arrival: &nbsp;
          <strong>
            <em>
              {formatDistanceToNow(new Date(schedule.next), {
                includeSeconds: true,
                addSuffix: true,
              })}
            </em>
          </strong>
        </p>
      </div>
    </div>
  ));

  if (!busStop) {
    return null;
  }
  return (
    <div className={styles.busStopScheduleContainer}>
      <div className={styles.header}>
        <h3>🚍 {busStop.name} station</h3>
        <button onClick={onClose}>❌</button>
      </div>
      <div className={styles.busStopScheduleList}>{cards}</div>
    </div>
  );
}
