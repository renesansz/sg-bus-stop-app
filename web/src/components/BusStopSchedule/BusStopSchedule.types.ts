import { BusStop } from '~/types';

export type BusStopScheduleProps = {
  busStop?: BusStop;
  onClose?: () => void;
};
