import { BusStop } from '~/types';

export type BusStopListProps = {
  busStops?: BusStop[];
  onClick?: (busStop: BusStop) => void;
};
