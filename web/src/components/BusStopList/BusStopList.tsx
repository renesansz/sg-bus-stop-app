import { useState } from 'react';
import styles from './BusStopList.module.css';
import { BusStopListProps } from './BusStopList.types';

export default function BusStopList(props: BusStopListProps) {
  const { busStops, onClick: onClickHandler } = props;

  const [minimized, setMinimized] = useState(false);

  const toggleMinimized = () => {
    setMinimized((prevVal) => !prevVal);
  };
  const cards = busStops.map((busStop) => (
    <div
      className={styles.busStopListItem}
      key={busStop.name}
      onClick={() => onClickHandler?.(busStop)}
    >
      <strong>{busStop.name}</strong>
    </div>
  ));

  return (
    <div className={styles.busStopListContainer}>
      <div className={styles.header}>
        <h3>🚏 {busStops.length} bus stops nearby</h3>
        <button onClick={toggleMinimized}>{minimized ? '🔻' : '🔺'}</button>
      </div>
      {!minimized && (
        <div className={styles.busStopsList}>
          {cards.length > 0 && <em>NOTE: Click on the item to view schedule</em>}
          {cards}
          {cards.length === 0 && <div className={styles.busStopListItem}>No result.</div>}
        </div>
      )}
    </div>
  );
}
