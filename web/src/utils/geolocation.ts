export function getLocation(): Promise<GeolocationPosition> {
  if (navigator.geolocation) {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (pos) => {
          resolve(pos);
        },
        (error) => {
          reject(error);
        },
      );
    });
  }
  throw new Error('Geolocation is not supported in this device.');
}
