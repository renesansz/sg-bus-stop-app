import { getAuth, signInAnonymously, UserCredential } from 'firebase/auth';
import { getFunctions, httpsCallable } from 'firebase/functions';

/**
 * Authenticate anonymous user to Firebase.
 */
export async function authenticate(): Promise<UserCredential> {
  const auth = getAuth();

  return await signInAnonymously(auth);
}

/**
 * Factory method to make reference to a "callable" HTTP trigger in Google Cloud Functions.
 *
 * Example usage:
 * ```
 * const firebaseFn = makeCallableFn("helloWorld")
 * firebaseFn() // Calls the `helloWorld` Firebase cloud function
 * ```
 *
 * @param fnName {string}
 * @returns HttpsCallable
 */
export function makeCallableFn<RequestData, ResponseData>(fnName: string) {
  const firebaseFns = getFunctions();

  return httpsCallable<RequestData, ResponseData>(firebaseFns, fnName);
}
