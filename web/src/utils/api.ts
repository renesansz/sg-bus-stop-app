import { getAuth } from 'firebase/auth';
import { getFunctions } from 'firebase/functions';

const {
  VITE_FIREBASE_USE_EMULATOR,
  VITE_FIREBASE_FUNCTIONS_EMULATOR_URL,
  VITE_FIREBASE_PROJECT_ID,
  VITE_API_URL,
} = import.meta.env;

const isEmulated = VITE_FIREBASE_USE_EMULATOR === 'true';
const API_URL = isEmulated ? VITE_FIREBASE_FUNCTIONS_EMULATOR_URL : VITE_API_URL;

/**
 * Factory method to build `fetch` request with Firebase authentication.
 *
 * @param path
 * @returns
 */
export function makeFetchApi(path: string, method: string = 'GET') {
  const firebaseFns = getFunctions();
  const auth = getAuth();
  const url = isEmulated
    ? `http://${API_URL}/${VITE_FIREBASE_PROJECT_ID}/${firebaseFns.region}/api/${path}`
    : `${API_URL}/${path}`;
  const fetchURL = new URL(url);

  return async (data: object | null = null) => {
    try {
      const token = await auth.currentUser?.getIdToken();

      if (data) {
        Object.keys(data).forEach((key) => {
          fetchURL.searchParams.set(key, data[key]);
        });
      }

      const response = fetch(fetchURL, {
        method,
        headers: {
          Authorization: `Bearer: ${token}`,
        },
      });

      return (await response).json();
    } catch (error) {
      throw new Error(error);
    }
  };
}
