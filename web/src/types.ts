import { BusStop } from '~/types';

export type LatLng = [number, number];
export type AppStates = {
  busStops: BusStop[];
  selectedBusStop: BusStop | null;
  center: LatLng; // Lat, Long
};
