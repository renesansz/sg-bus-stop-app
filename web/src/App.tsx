import Map from '@/components/Map';
import { authenticate } from '@/utils/firebase';
import { Component, ReactNode } from 'react';
import { BusStop } from '~/types';
import './App.css';
import BusStopList from './components/BusStopList/BusStopList';
import BusStopSchedule from './components/BusStopSchedule';
import { AppStates, LatLng } from './types';
import { makeFetchApi } from './utils/api';
import { getLocation } from './utils/geolocation';

let queryDebounce;

class App extends Component<object, AppStates> {
  constructor(props: object) {
    super(props);

    this.state = {
      busStops: [],
      selectedBusStop: null,
      center: [1.2903, 103.847],
    };
    this.onBusStopListClick = this.onBusStopListClick.bind(this);
    this.fetchNearestBusStop = this.fetchNearestBusStop.bind(this);
    this.onDragEndHandler = this.onDragEndHandler.bind(this);
    this.doCloseScheduleModal = this.doCloseScheduleModal.bind(this);
  }

  componentDidMount() {
    getLocation().then((pos) => {
      this.setState({
        center: [pos.coords.latitude, pos.coords.longitude],
      });
      this.fetchNearestBusStop();
    });
    authenticate().then(() => {
      this.fetchNearestBusStop();
    });
  }

  async fetchNearestBusStop(center?: LatLng) {
    const getBusStops = makeFetchApi('bus-stops');

    const busStops = await getBusStops({
      lat: center?.[0] ?? this.state.center[0],
      long: center?.[1] ?? this.state.center[1],
    });

    this.setState({
      busStops,
    });
  }

  onBusStopListClick(busStop: BusStop) {
    this.setState({
      selectedBusStop: busStop,
      center: [busStop.coordinates.lat, busStop.coordinates.long],
    });
  }

  doCloseScheduleModal() {
    this.setState({
      selectedBusStop: null,
    });
  }

  onDragEndHandler(center: LatLng) {
    this.setState({
      center,
    });

    if (queryDebounce) {
      clearTimeout(queryDebounce);
    }

    queryDebounce = setTimeout(() => {
      this.fetchNearestBusStop(center);
    }, 2000);
  }

  render(): ReactNode {
    return (
      <>
        <BusStopList busStops={this.state.busStops} onClick={this.onBusStopListClick} />
        <BusStopSchedule busStop={this.state.selectedBusStop} onClose={this.doCloseScheduleModal} />
        <Map
          center={this.state.center}
          busStops={this.state.busStops}
          onDragEnd={this.onDragEndHandler}
        />
      </>
    );
  }
}
export default App;
