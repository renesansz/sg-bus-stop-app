import { FirebaseApp, initializeApp } from 'firebase/app';
import { Auth, connectAuthEmulator, getAuth } from 'firebase/auth';
import { Functions, connectFunctionsEmulator, getFunctions } from 'firebase/functions';

const {
  VITE_FIREBASE_API_KEY,
  VITE_FIREBASE_AUTH_DOMAIN,
  VITE_FIREBASE_PROJECT_ID,
  VITE_FIREBASE_APP_ID,
  VITE_FIREBASE_USE_EMULATOR,
  VITE_FIREBASE_FUNCTIONS_EMULATOR_URL,
} = import.meta.env;

export type FirebaseInstance = {
  app: FirebaseApp;
  auth: Auth;
  functions: Functions;
};

export function initFirebase(): FirebaseInstance {
  const app = initializeApp({
    apiKey: VITE_FIREBASE_API_KEY,
    authDomain: VITE_FIREBASE_AUTH_DOMAIN,
    projectId: VITE_FIREBASE_PROJECT_ID,
    appId: VITE_FIREBASE_APP_ID,
  });
  const auth = getAuth(app);
  const functions = getFunctions(app);

  if (VITE_FIREBASE_USE_EMULATOR === 'true') {
    const [fnHost, fnPort]: string[] = VITE_FIREBASE_FUNCTIONS_EMULATOR_URL.split(':');

    connectAuthEmulator(auth, 'http://127.0.0.1:9099');
    connectFunctionsEmulator(functions, fnHost, Number(fnPort));
  }

  return {
    app,
    auth,
    functions,
  };
}
