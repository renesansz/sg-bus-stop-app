setup:
	curl -fsSL https://bun.sh/install | bash
	curl -sL https://firebase.tools | bash

init-functions:
	cd ./functions && bun install

init-web:
	cd ./web && bun install

init:
	make init-functions
	make init-web

build-functions:
	cd functions && bun run build

build-web:
	cd web && bun run build

build:
	make -j4 build-web build-functions

run-functions:
	cd functions && bun run serve

run-web:
	cd web && VITE_FIREBASE_USE_EMULATOR=true bun dev

run-web-prod:
	cd web && VITE_FIREBASE_USE_EMULATOR=false bun dev

run-firebase-emulators:
	make build-functions
	firebase emulators:start

run-firebase-emulators-debug:
	make build-functions
	firebase emulators:start --debug

run:
	make -j4 run-web run-firebase-emulators
