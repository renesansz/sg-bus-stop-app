// Derived types based on API: https://arrivelah2.busrouter.sg/

export type BusNextStop = {
  time: string;
  duration_ms: number;
  lat: number;
  lng: number;
  load: string;
  feature: string;
  type: string;
  visit_number: number;
  origin_code: string;
  destination_code: string;
}

export type NextBusScheduleApiResponse = {
  no: string;
  operator: string;
  next: BusNextStop | null;
  subsequent: BusNextStop | null;
  next2: BusNextStop | null;
  next3: BusNextStop | null;
}
