export type BusEntity = {
  no: string;
  name: string;
}

export type BusStop = {
  id: string;
  name: string;
  geohash?: string;
  coordinates: {
    lat: number;
    long: number;
  };
};

export type BusStopScheduleService = {
  bus: BusEntity;
  operator: string;
  next: string;
}

export type BusStopSchedules = {
  [key: string]: {
    services: BusStopScheduleService[]
  }
}

export type GetBusStopsArrivalFormData = {
  coordinates: {
    lat: number;
    long: number;
  };
};
