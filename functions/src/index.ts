import { onRequest } from 'firebase-functions/v2/https';
import expressApp from './api';
export * from './migrations';

export const api = onRequest({ cors: true }, (req, res) => {
  if (!req.path) {
    req.url = `/${req.url}`; // Prepend '/' to keep query params if any
  }
  return expressApp(req, res);
});
