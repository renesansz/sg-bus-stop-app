import express from 'express';
import helmet from 'helmet';
import { getBusStops, getBusStopSchedule } from './handlers/bus-stops';
import { checkAuth } from './middleware';

const api = express();

api.use(
  helmet({
    // NOTE: We handle CORS in Firebase Cloud Functions instead.
    contentSecurityPolicy: false,
  }),
);
api.use(checkAuth);

api.get('/bus-stops', getBusStops);
api.get('/bus-stops/:busStopId', getBusStopSchedule);

export default api;
