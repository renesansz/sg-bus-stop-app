import { getAuthInstance } from '../utils/firebase-admin';
import { logger, makeMessagePrefix } from '../utils/logger';

const prefixMsg = makeMessagePrefix('checkAuth');

const getAuthToken = (req) => {
  const authHeader = req.headers.authorization.split(': ');

  if (req.headers.authorization && authHeader[0] === 'Bearer') {
    return authHeader[1];
  }
  return null;
};

export const checkAuth = async (req, res, next) => {
  try {
    const token = getAuthToken(req);
    const firebaseAuth = getAuthInstance();

    await firebaseAuth.verifyIdToken(token);

    next();
  } catch (error) {
    logger.error(prefixMsg(`Error: ${error}`));
    res.status(401).send({ message: 'You are not authorized to make this request' });
  }
};
