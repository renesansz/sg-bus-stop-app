import { RequestHandler } from 'express';
import { distanceBetween, geohashQueryBounds, Geopoint } from 'geofire-common';
import { BusStop } from '~/types';
import { getFirestoreInstance } from '../../utils/firebase-admin';
import { logger, makeMessagePrefix } from '../../utils/logger';

const prefixMsg = makeMessagePrefix('bus-stops');

/**
 * Query nearest bus stops within 3km using Query Geohashes.
 *
 * See: https://firebase.google.com/docs/firestore/solutions/geoqueries#web-namespaced-api
 */
export const getBusStops: RequestHandler = async (req, res) => {
  try {
    const db = getFirestoreInstance();

    const { lat, long } = req.query;

    const userLocation: Geopoint = [Number(lat), Number(long)];
    const radiusInM = 3 * 1000; // Nearest within 3km
    const bounds = geohashQueryBounds(userLocation, radiusInM);

    const dbQueries = bounds.map((bound) => {
      const query = db.collection('bus-stops').orderBy('geohash').startAt(bound[0]).endAt(bound[1]);
      return query.get();
    });

    const snapshots = await Promise.all(dbQueries);
    const result = snapshots.reduce((acc: BusStop[], snapshot) => {
      for (const doc of snapshot.docs) {
        const lat = doc.get('coordinates.lat');
        const long = doc.get('coordinates.long');

        const distanceInKm = distanceBetween([lat, long], userLocation);
        const distanceInM = distanceInKm * 1000;

        if (distanceInM <= radiusInM) {
          acc.push(doc.data() as BusStop);
        }
      }

      return acc;
    }, []);

    res.status(200).send(result);
  } catch (error) {
    logger.error(prefixMsg('getBusStops error: ${error}'));
    res.status(500).send('An error has occured.');
  }
};

export const getBusStopSchedule: RequestHandler = async (req, res) => {
  const { busStopId } = req.params;

  try {
    const db = getFirestoreInstance();
    const query = db.collection('bus-stop-schedules').doc(busStopId);
    const result = (await query.get()).data();

    res.status(200).send(result);
  } catch (error) {
    logger.error(prefixMsg(`getBusStopSchedule error: ${error}`));
    res.status(500).send('An error has occured.');
  }
};
