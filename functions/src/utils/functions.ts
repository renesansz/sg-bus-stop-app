import { CallableFunction, CallableRequest, onCall } from 'firebase-functions/v2/https';

/**
 * Factory method to create `onCall` method with `enforceAppCheck: enabled` by default.
 *
 * Documentation: https://firebase.google.com/docs/app-check
 *
 * @param handler callback function
 * @returns function
 */
export function makeProtectedFn<RequestData, Return>(
  handler: (request: CallableRequest<RequestData>) => Return,
): CallableFunction<RequestData, Return> {
  return onCall(
    {
      // NOTE: AppCheck bug when using Firebase emulator, causing "valid" anonymous auths to fail.
      // https://github.com/firebase/firebase-tools/issues/5253
      enforceAppCheck: process.env.FUNCTIONS_EMULATOR !== 'true',
    },
    handler,
  );
}
