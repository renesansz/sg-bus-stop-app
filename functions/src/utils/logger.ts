export * as logger from 'firebase-functions/logger';

export const makeMessagePrefix = (prefix: string) => (message: string) => `[${prefix}]: ${message}`;
