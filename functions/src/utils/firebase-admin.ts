import admin from 'firebase-admin';
import { App } from 'firebase-admin/app';
import { getAuth } from 'firebase-admin/auth';
import { getFirestore } from 'firebase-admin/firestore';
import { logger, makeMessagePrefix } from './logger';

let app: App;

const prefixMsg = makeMessagePrefix('firebase-admin');

export function getAppInstance() {
  if (!app) {
    logger.info(prefixMsg('No existing app yet, initializing...'));
    app = admin.initializeApp({
      credential: admin.credential.applicationDefault(),
    });
    logger.info(prefixMsg('App initialized!'));
  }

  return app;
}

export function getFirestoreInstance() {
  const app = getAppInstance();
  return getFirestore(app);
}

export function getAuthInstance() {
  const app = getAppInstance();
  return getAuth(app);
}
