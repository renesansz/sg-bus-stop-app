import { Firestore } from 'firebase-admin/firestore';
import { HttpsError } from 'firebase-functions/v2/https';
import { geohashForLocation } from 'geofire-common';
import { BusStop, BusStopSchedules } from '~/types';
import busStopScheduleFixtures from '../../shared/fixtures/bus-stop-schedules.json';
import busStopFixtures from '../../shared/fixtures/bus-stops.json';
import { getFirestoreInstance } from './utils/firebase-admin';
import { makeProtectedFn } from './utils/functions';
import { logger, makeMessagePrefix } from './utils/logger';

const prefixMsg = makeMessagePrefix('migrations');

export const migrations = makeProtectedFn(async () => {
  logger.info(prefixMsg('Running migrations...'));
  try {
    const db = getFirestoreInstance();

    await migrateBusStops(db);
    await migrateBusStopSchedules(db);

    return 'Successfully run migrations.';
  } catch (error) {
    logger.error(prefixMsg(`Error: ${error}`));
    throw new HttpsError('internal', 'An error has occured.', error);
  }
});

async function migrateBusStops(db: Firestore) {
  try {
    const busStopsCollection = db.collection('bus-stops');
    const busStopCollectionSnap = await busStopsCollection.count().get();

    if (busStopCollectionSnap.data().count === 0) {
      logger.info(prefixMsg('"bus-stops" collections is empty, importing fixtures...'));

      const writeBatch = db.batch();
      const busStopsFixture: BusStop[] = busStopFixtures;

      busStopsFixture.forEach((data) => {
        const docRef = busStopsCollection.doc(data.id);
        const geohash = geohashForLocation([data.coordinates.lat, data.coordinates.long]);

        writeBatch.create(docRef, {
          ...data,
          geohash,
        });
      });

      await writeBatch.commit();

      logger.info(
        prefixMsg(`Successfully imported ${busStopsFixture.length} "bus-stops" records.`),
      );
    } else {
      logger.info(prefixMsg('"bus-stops" collections has data already, skipping migration...'));
    }
  } catch (error) {
    logger.error(prefixMsg(`Error: ${error}`));
    throw new HttpsError('internal', 'An error has occured.', error);
  }
}

async function migrateBusStopSchedules(db: Firestore) {
  try {
    const busStopSchedulesCollection = db.collection('bus-stop-schedules');
    const busStopScheduleCollectionSnap = await busStopSchedulesCollection.count().get();

    if (busStopScheduleCollectionSnap.data().count === 0) {
      logger.info(prefixMsg('"bus-stop-schedules" collections is empty, importing fixtures...'));

      const writeBatch = db.batch();
      const busStopScheduleFixture: BusStopSchedules = busStopScheduleFixtures;
      const busStopIds = Object.keys(busStopScheduleFixture);

      busStopIds.forEach((busStopId) => {
        const docRef = busStopSchedulesCollection.doc(busStopId);

        writeBatch.create(docRef, busStopScheduleFixture[busStopId]);
      });

      await writeBatch.commit();

      logger.info(
        prefixMsg(`Successfully imported ${busStopIds.length} "bus-stop-schedules" records.`),
      );
    } else {
      logger.info(
        prefixMsg('"bus-stop-schedules" collections has data already, skipping migration...'),
      );
    }
  } catch (error) {
    logger.error(prefixMsg(`Error: ${error}`));
    throw new HttpsError('internal', 'An error has occured.', error);
  }
}
