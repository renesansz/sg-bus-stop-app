# SG-BUS-STOP-APP

Demo: https://sg-bus-stop-app.web.app/

## Setup

#### Prerequisites

Make sure to have the following installed in local for _Firebase emulators_ to work:
* Node.js version 16.0 or higher.
* Java JDK version 11 or higher.

#### Run `make setup` to install local setup dependencies.

_Note: Installation will require you to input your password_

#### Local Development

##### Initialize project dependencies

* Run `make init` to install project dependencies.

* Create a `.env` file inside `web` directory. (see `.env.tpl` for reference)

##### Running the app

* `make run` - To run both _web app_ and _**emulated** Firebase services_.
* `make run-web-prod` - Run web app and connect to **actual** Firebase.

_Additional commands:_
* `make run-web` - Run web app using the Firebase emulator.
* `make run-functions` - Run emulated _cloud functions_ service **ONLY**
* `make run-firebase-emulators` - Run emulated Firebase services in _local_ (`Cloud functions`, `Auth`, `Firestore`)
* `make build` - Build both _web app_ & _cloud functions_ for **production** deployment
* `make build-web` - Build web app for production env
* `make build-functions` - Build functions to be deployable to Firebase

##### Initializing local Firestore data using Postman

_For local, make sure both `web` and `firebase-emulators` are running._

1. Open the web app and open up inspect element > network tab
2. Get your Bearer token by filtering by `identitytoolkit.googleapis.com` request then check the **Payload** tab and copy the `idToken`
3. On Postman, create a POST request to `http://127.0.0.1:5001/sg-bus-stop-app/us-central1/migrations` then set the **Authorization** > _Bearer Token_ using the value you've copied on the previous step. Then, set the **request body** > raw > `{ data: null }` (_VERY_ important)
4. Send the request and you should get a success response.
